//Import thư viện express js tương đương import express from "express"
const express = require("express");

//Khởi tạo app express
const app = express();

//Khai báo cổng chạy project
const port = 8000;

//Callback function lá 1 function đóng vai trò là tham số của 1 function khác, nó sẽ được thực hiện khi function chủ được gọi
//Khai báo API dạng /
app.get("/api", (req, res) => {
    let today = new Date();
    res.json({
        message: `Xin chào hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

app.listen(port, () => {
    console.log("App listening on port: ", port);
})

//khai báo api dạng GET
app.get("/get-method", (req, res) => {
    res.json({
        message: "GET method"    
    })
})
//khai báo api dạng POST
app.get("/post-method", (req, res) => {
    res.json({
        message: "POST method"    
    })
})
//khai báo api dạng PUT
app.get("/put-method", (req, res) => {
    res.json({
        message: "PUT method"    
    })
})
//khai báo api dạng DELETE
app.get("/delete-method", (req, res) => {
    res.json({
        message: "DELETE method"    
    })
})
